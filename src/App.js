import React, { Component } from 'react';

import Header from './components/header'
import StatusBar from './components/statusBar'
import Product from './components/productList'

import './App.css';

class App extends Component {
	constructor(){
		super();
		this.state ={
			contador: 0,
			productExibition: 'square',
			products: [
				{bookmark: false},
				{bookmark: false},
				{bookmark: false},
				{bookmark: false},
				{bookmark: false}
			],
			viewSquare: true,
			viewDefault: false,
			viewGrid: false,

		}
	}

	changeView = (view) => {
		if(view === 'square'){
			return(
				this.setState({
					productExibition: 'square',
					viewSquare: true,
					viewDefault: false,
					viewGrid: false,
				})
			)
		}
		if(view === 'square'){
			return(
				this.setState({
					productExibition: 'square',
					viewSquare: true,
					viewDefault: false,
					viewGrid: false,
				})
			)
		}
		if(view === 'list'){
			return(
				this.setState({
					productExibition: 'list',
					viewSquare: false,
					viewDefault: true,
					viewGrid: false,
				})
			)
		}
		if(view === 'grid'){
			return(
				this.setState({
					productExibition: 'grid',
					viewSquare: false,
					viewDefault: false,
					viewGrid: true,
				})
			)
		}
	}

	render() {
		return (
			<div className="App">
				<Header count={this.state.contador} />
				<StatusBar
					squareList={() => this.changeView('square')}
					squareAtive={this.state.viewSquare}
					defaultList={() => this.changeView('list')}
					defaultAtive={this.state.viewDefault}
					gridList={() => this.changeView('grid')}
					gridAtive={this.state.viewGrid}
				/>
				<div className={"ProductsList " + this.state.productExibition}>
					{
						this.state.products.map((product, i) => {
							return(
								<Product key={i}
									favoriteAdd={
										() => {
											if(product.bookmark === false){
												let produ  = this.state.products;
												produ[i].bookmark = true
												return(
													this.setState({
														contador: this.state.contador +1,
														products: produ
													})
												)
											}else{
												let produ  = this.state.products;
												produ[i].bookmark = false
												return(
													this.setState({
														contador: this.state.contador - 1,
														products: produ
													})
												)
											}
										}
									}
								/>
							)
						})
					}
				</div>
			</div>
		);
	}
}

export default App;
