import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-regular-svg-icons';

export default class Favorite extends Component {
	constructor(){
		super();
		this.state ={
			favoriteActive: false
		}
	}
	render() {
		return (
			<div
				className={this.state.favoriteActive ? 'Product__whishList active' : 'Product__whishList'}
				onClick={this.props.favoriteAdd}
				onClickCapture={() => this.setState({favoriteActive: !this.state.favoriteActive})}>
				<p> Add to whishlist</p>
				<FontAwesomeIcon icon={faHeart} size="lg"/>
			</div>
		)
	}
}
