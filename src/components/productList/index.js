import React, { Component } from 'react'
import Favorite from './favorite';

export default class Product extends Component {
	render() {
		return (
				<div className="ProductList__item Product">
					<div className="Product__image">
						<img src={"http://via.placeholder.com/450x300"} alt="Logo"/>
					</div>
					<div className="product-control">
						<div className="Product__title">
							<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta est sed luctus maximus. </h2>
						</div>
						<div className="Product__price">
							<div className="Price__full">
								R$ 0000,00 <span> à vista</span>
							</div>
							<div className="Price__card">
								R$ 0000,00 <span> em (X)x </span>
								R$ 0000,00 s/ juros
							</div>
						</div>
						<div className="Product__sale">
							tag %%
						</div>
						<Favorite favoriteActive={this.props.favoriteActive} favoriteAdd={this.props.favoriteAdd}/>
					</div>
				</div>
		)
	}
}
