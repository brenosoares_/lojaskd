import React, { Component } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch} from '@fortawesome/free-solid-svg-icons';

export default class SearchBar extends Component {
	render() {
		return (
			<div className="Header__searchbar Searchbar">
				<input type="text" placeholder="Search field..." className="Searchbar__input" />
				<button className="Searchbar__btn"><FontAwesomeIcon icon={faSearch} size="2x" /></button>
		  </div>
		)
	}
}
