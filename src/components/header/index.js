import React, { Component } from 'react'
import ToolBar from './toolbar'
import SearchBar from './searchBar'
import TagCloud from './tagCloud'

export default class Header extends Component {
  render() {
	return (
		<header className="App__header Header">
			<ToolBar count={this.props.count} />
			<SearchBar />
			<TagCloud />
		</header>
	)
  }
}
