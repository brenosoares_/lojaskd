import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faUser, faShoppingCart, faHeart } from '@fortawesome/free-solid-svg-icons';

export default class ToolBar extends Component {

	render() {
		return (
			<div className="Header__toolbar Toolbar">
				<div className="Toolbar__menuBtn MenuBtn">
					<FontAwesomeIcon icon={faBars} size="2x" />
				</div>
				<div className="Toolbar__logo Logo">
					<img src={"http://via.placeholder.com/120x32"} alt="Logo"/>
				</div>
					<div className="Toolbar__favorite Favorite">
						<FontAwesomeIcon icon={faHeart} size="2x"/>
				{this.props.count > 0 ?
						<div className="Favorite__count Count">
						{this.props.count}
						</div>
				 : null}
					</div>
				<div className="Toolbar__user User">
					<FontAwesomeIcon icon={faUser} size="2x" />
				</div>
				<div className="Toolbar__cart Cart">
					<FontAwesomeIcon icon={faShoppingCart} size="2x" />
				</div>
		  </div>
		)
	}
}
