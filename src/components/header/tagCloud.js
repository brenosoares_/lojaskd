import React, { Component } from 'react'

export default class TagCloud extends Component {
	render() {
		return (
			<div className="Header__tagCloud TagCloud">
				<div className="TagCloud__tagList TagList">
					<div className="TagList__tag Tag">This is a tag</div>
					<div className="TagList__tag Tag">This is a tag</div>
					<div className="TagList__tag Tag">This is a tag</div>
					<div className="TagList__tag Tag">This is a tag</div>
					<div className="TagList__tag Tag">This is a tag</div>
					<div className="TagList__tag Tag">this is a tag</div>
				</div>
			</div>
		)
	}
}
