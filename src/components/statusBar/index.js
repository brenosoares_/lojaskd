import React, { Component } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSquare, faList, faTh } from '@fortawesome/free-solid-svg-icons';

export default class StatusBar extends Component {
	render() {
		return (
			<div className="StatusBar">
				<div className="StatusBar__results Results">
					<p className="Results__number">9999</p>
					<p>resultados</p>
				</div>
				<div className="StatusBar__exibition Exibition">
					<p>modo de exibição</p>
					<div className={this.props.squareAtive ? 'Exibition__items Full  active' : "Exibition__items Full "} onClick={this.props.squareList}>
						<FontAwesomeIcon icon={faSquare} size="1x" />
					</div>
					<div className={this.props.defaultAtive ? 'Exibition__items List active' : "Exibition__items List "} onClick={this.props.defaultList}>
						<FontAwesomeIcon icon={faList} size="1x" />
					</div>
					<div className={this.props.gridAtive ? 'Exibition__items Grid active' : "Exibition__items Grid "} onClick={this.props.gridList}>
						<FontAwesomeIcon icon={faTh} size="1x" />
					</div>
				</div>
			</div>
		)
	}
}
